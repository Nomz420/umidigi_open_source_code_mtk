// Copyright Statement:
//
// This software/firmware and related documentation ("MediaTek Software") are
// protected under relevant copyright laws. The information contained herein
// is confidential and proprietary to MediaTek Inc. and/or its licensors.
// Without the prior written permission of MediaTek inc. and/or its licensors,
// any reproduction, modification, use or disclosure of MediaTek Software,
// and information contained herein, in whole or in part, shall be strictly prohibited.
//
// MediaTek Inc. (C) 2017. All rights reserved.
//
// BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
// THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
// RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
// AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
// NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
// SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
// SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
// THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
// THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
// CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
// SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
// STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
// CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
// AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
// OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
// MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
//
// The following software/firmware and/or related documentation ("MediaTek Software")
// have been modified by MediaTek Inc. All revisions are subject to any receiver's
// applicable license agreements with MediaTek Inc.

bootstrap_go_package {
    name: "soong-TelephonyBase-mediatek",
    pkgPath: "android/soong/TelephonyBase/mediatek",
    deps: [
        "soong-android",
        "soong-java",
    ],
    srcs: [
       "TelephonyBase.go",
    ],
    pluginFor: ["soong_build"],
}

mtk_TelephonyBase_defaults {
    name: "mediatek-TelephonyBase_defaults",
}

java_library {
    name: "mediatek-telephony-base",
    installable: true,

    defaults: [
        "mediatek-TelephonyBase_defaults",
    ],

    aidl: {
        local_include_dirs: ["java"],
        include_dirs: ["frameworks/native/aidl/gui"],
    },

    srcs: ["java//**/*.java"] + [
        "java/com/mediatek/internal/telephony/IMtkTelephonyEx.aidl",
        "java/com/mediatek/internal/telephony/IMtkPhoneSubInfoEx.aidl",
        "java/com/mediatek/internal/telephony/IMtkSms.aidl",
        "java/com/mediatek/internal/telephony/IMtkSub.aidl",
        "java/com/mediatek/gwsd/IGwsdListener.aidl",
        "java/com/mediatek/gwsd/IGwsdService.aidl",
        "java/com/mediatek/dm/IDmService.aidl",
    ],

    libs: [
        "mediatek-common",
        "framework",
        "mediatek-framework",
        "mediatek-ims-base",
    ],
}

// Include subdirectory makefiles
// ============================================================

filegroup {
    name: "mediatek-telephony-base-srcs",
    srcs: [
       "java/com/mediatek/internal/telephony/*.java",
       "java/com/mediatek/internal/telephony/MtkIccCardConstants.java",
       "java/com/mediatek/provider/MtkContactsContract.java",
       "java/mediatek/telephony/*.java",
       "java/com/mediatek/telephony/MtkTelephonyManagerEx.java",
       "java/com/mediatek/internal/telephony/IMtkTelephonyEx.aidl",
    ],
}

